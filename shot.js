var system = require('system');
var page = require('webpage').create();

if(system.args.length < 1) {
    console.log('Please provide a URL');
    phantom.exit();
}

var url = system.args[1];
var output = system.args[2] || 'shot.png';
var delay = system.args[3] || 0;

page.clipRect = {
  top: 0,
  left: 0,
  width: 1200,
  height: 800
};

page.viewportSize = { width: 1200, height: 800 };

page.zoomFactor = 0.8;

page.open(url, function(status) {
  console.log("Status: " + status);
  setTimeout(function() {
    page.render(output);
    phantom.exit();
  }, delay);
});
